#include "Lock.h"
#include <stdint.h>

#define GPIO_PORTB_DATA_R       (*((volatile unsigned long *)0x400053FC))

volatile unsigned long long loop;
char retCode;
    
void debounceLoop(){
    for(loop = 0; loop < 800000; loop++){;}
}

Lock::Lock()
{
    int i ;
    for(i = 0; i < pinCount; i++)
    {
        lockPins[i] = NULL;
    }    

    codeCombination = 0;
    status = 0;
}

char Lock::setCombination(int code)
{
    while(code>=1){
        codeCombination += (lockPins[code%10])->getValue();
        code /= 10;
    }
    
    return 0;
        
}

char Lock::setConfirmPin(char pin)
{
    if(pin >= pinCount){
        return 2;
    }
    
    if(lockPins[pin] != NULL){
        confirmPin = pin;
        retCode = 0;
    }
    else{
        retCode = 1;
    }
    
    return retCode;
}


void Lock::clearCombination(void){
    this->codeCombination = 0;
    
}

    
char Lock::checkConfirmPin(int inputVal){
    if(lockPins[confirmPin]->getValue() == inputVal){
        unlockLock();
        retCode = 0;
    }
    else{
        if(checkCombination(inputVal) != 0){
            lockLock();
        }
        retCode = 1;
    }
    return retCode;
}

char Lock::checkPin(char pos){
    return lockPins[pos] != NULL;
}

char Lock::addPin(LockPin& pin){
    
    if(pin.getKey() > pinCount){
        retCode = 1;
    }
    
    else if(checkPin(pin.getKey()) == 1){
        retCode = 2;
    }
    else{
        retCode = 0;
    }
     
    lockPins[pin.getKey()] = &pin;
   
    return retCode;
}

char Lock::checkCombination(int combination){
    if(combination == codeCombination){
        retCode = 0;
        status = 1;
    }
    else{
        lockLock();
        retCode = 1;
    }
    return retCode;
}

char Lock::unlockSequence(int inputVal){
    
    if(status == 0){
        checkCombination(inputVal);
        retCode = 0;
    }
    else if(status == 1){
        checkConfirmPin(inputVal);
        retCode = 1;
    }
    else{
        lockLock();
        retCode = 2;
    }
    debounceLoop();
    return retCode;
}

char Lock::unlockLock(void){
    if(status == 2){
        return 1;
    }
    GPIO_PORTB_DATA_R |= 0x1;
    status = 2;
    debounceLoop();
    return 0;
}

char Lock::lockLock(void){
    if(status == 0){
        return 1;
    }
    GPIO_PORTB_DATA_R &= ~(0x1);
    status = 0;
    debounceLoop();
    return 0;
}
#include "Lock.h"
#include <stdint.h>

#define GPIO_PORTB_DATA_R       (*((volatile unsigned long *)0x400053FC))

volatile unsigned long long loop;

void debounceLoop(){
    for(loop = 0; loop < 700000; loop++){;}
}

Lock::Lock()
{
    int i ;
    for(i = 0; i < pinCount; i++)
    {
        this->lockPins[i] = NULL;
    }    
    for(i = 0; i < maxCodeLength; i++)
    {
        this->code[i] = -1;
    }    
    this->codeLength = 0;
    this->status = 0;
}

char Lock::setCode(int code)
{

    int mul = 1;
    char temp;
    
    while(mul < code){
        mul *= 10;
    }
    
    if(mul != 1){
        mul /= 10;
    }
        
    while(mul>=1){
        
        temp = (code/mul) % 10;
        
        if(temp > pinCount) return 1;
        
        if((lockPins[temp]) == NULL ){
            this->clearCode();
            return 2;
        }
        this->addCodeDigit(temp);

        mul /= 10;
    }
    
    return 0;
        
}

void Lock::clearCode(void){
    for(int i = 0; i < maxCodeLength; i++)
    {
        code[i] = -1;
    }    
    this->codeLength = 0;
}

char Lock::addCodeDigit(char digit){
    
    if((int)this->codeLength >= maxCodeLength){
        this->clearCode();
        return 1;
    }
        
    this->code[this->codeLength] = digit;
    this->codeLength++;
    
    return 0;
}

char Lock::getCodeLength(void){
    return this->codeLength;
}
    
char Lock::checkPin(char pos){
    return lockPins[pos] != NULL;
}

char Lock::addPin(LockPin& pin){
       
    if(pin.getKey() > pinCount){
        return 1;
    }
    
    if(checkPin(pin.getKey()) == 1){
        return 2;
    }
        
    lockPins[pin.getKey()] = &pin;
    return 0;
}


char Lock::unlockSequence(void){
    volatile char sequenceStep = 0;
    char success;
    volatile unsigned long long time = 0;
    
    while(sequenceStep < this->codeLength){
        time = 0;
        success = 0;
        
        
        while(time < timeout){
            if(this->checkPin(code[sequenceStep]) == 0){
                this->lockLock();
                return 2;
            }
            
            if( (this->lockPins[code[sequenceStep]])->getStatus() ){
                sequenceStep++;
                success = 1;
                
                while((this->lockPins[code[sequenceStep]])->getStatus()){;}
                debounceLoop();

                break;
            }
            
            else if( (GPIO_PORTB_DATA_R & ~0x1) != 0){
                this->lockLock();
                debounceLoop();
                return 3;
            }
                        
            time++;
        }
        
        if(success == 0){
            debounceLoop();
            return 1;
        } 
    }    
    this->unlockLock();
    debounceLoop();
    return 0;
}

char Lock::unlockLock(void){
    if(this->status != 0){
        return 1;
    }
    GPIO_PORTB_DATA_R = 0x1;
    this->status = 1;
    return 0;
}

char Lock::lockLock(void){
    if(this->status != 1){
        return 1;
    }
    GPIO_PORTB_DATA_R &= ~(0x1);
    this->status = 0;
    return 0;
}
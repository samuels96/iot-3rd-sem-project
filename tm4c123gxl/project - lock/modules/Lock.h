
#include "LockPin.h"
#include <vector>
#include <string>

#define pinCount 8
#define maxCodeLength 32
#define timeout 9999999

class Lock{
    private:
        char status; // 0: Locked | 1: Waiting for confirmation pin | 2: Unlocked
        char confirmPin;
        unsigned char codeCombination;
        LockPin* lockPins[pinCount];
    public:
        Lock();
        char unlockSequence(int inputVal);
        char setCombination(int code);
        char setConfirmPin(char pin);
        void clearCombination(void);
        char addPin(LockPin& pin);
        char checkPin(char pos);
        char checkConfirmPin(int inputVal);
        char checkCombination(int combination);
        char unlockLock(void);
        char lockLock(void);
};
